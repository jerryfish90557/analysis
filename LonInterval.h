#ifndef _LONINTERVAL_H
#define _LONINTERVAL_H

#define Lon_min 119.
#define Lon_max 123.
#define Lat_min 21.5
#define Lat_max 26.
#define Lon_bin 100

#include <array>
#include "LoadFile.h"
#include "StrangeCh.h"
#include "Normalization.h"
#include "polar.h"

extern bool strange;
extern vector<int> vecNewCh;
extern vector<double> vecBestScan;
extern bool pass;

extern TFile* outputroot;
extern vector<vector<int> > VecAve;
extern vector<vector<int> > VecCoin;
extern vector<vector<double> > VecPolLon;
extern vector<vector<double> > VecPolLat;

Long64_t temp_lon_dur, temp_pol_dur;
double temp_lon, temp_pol_lon, temp_lat = -10000 ,temp_pol_lat = -10000;
double Lon_binw = (Lon_max-Lon_min)/(double)Lon_bin;
double Lat_binw = (Lat_max-Lat_min)/(double)Lon_bin;
double pol_lon_binw = (Lon_max-Lon_min)/(double)pol_lon_bin;
double pol_lat_binw = (Lat_max-Lat_min)/(double)pol_lat_bin;
int data_lon, data_lat, next_lon, next_lat;
int pol_lon, pol_lat, pol_next_lon, pol_next_lat;
vector<vector<Long64_t> > VecDur(Lon_bin, vector<Long64_t>(Lon_bin,0));
vector<vector<double> > VecCoinN(Lon_bin, vector<double>(Lon_bin,0));

array<array<Long64_t, pol_lat_bin>, pol_lon_bin> ArrPolDur;

void LonInterval(Astra &data, Astra &latter, double adjust, vector<int> &vecChID)
{
	data_lon = (data.Lon-Lon_min)/Lon_binw;
	data_lat = (data.Lat-Lat_min)/Lat_binw;
	next_lon = (latter.Lon-Lon_min)/Lon_binw;
	next_lat = (latter.Lat-Lat_min)/Lat_binw;

	pol_lon = (data.Lon-Lon_min)/pol_lon_binw;
	pol_lat = (data.Lat-Lat_min)/pol_lat_binw;
	pol_next_lon = (latter.Lon-Lon_min)/pol_lon_binw;
	pol_next_lat = (latter.Lat-Lat_min)/pol_lat_binw;

	double Nfactor = 1./Normalize(data.Alt);

	if(strange == true)
	{
		if(vecNewCh.size()>=2)
		{
			if(pass == true)
			{
				VecCoinN[data_lon][data_lat] = VecCoinN[data_lon][data_lat] + Nfactor;
				VecCoin[data_lon][data_lat]++;
				if(vecNewCh.size()==2)
				{ polar(vecDirSph, pol_lon, pol_lat, adjust, Nfactor); }
				else
				{ polar(vecBestScan, pol_lon, pol_lat, adjust, Nfactor); }
			}
		}
	}
	else
	{
		if(vecChID.size()>=2)
		{
			if(pass == true)
			{
				VecCoinN[data_lon][data_lat] = VecCoinN[data_lon][data_lat] + Nfactor;
				VecCoin[data_lon][data_lat]++;
				if(vecChID.size()==2)
				{ polar(vecDirSph, pol_lon, pol_lat, adjust, Nfactor); }
				else
				{ polar(vecBestScan, pol_lon, pol_lat, adjust, Nfactor); }
			}
		}
	}

	if(temp_lat == -10000)
	{
		temp_lon = data_lon;
		temp_lat = data_lat;
		temp_lon_dur = data.gtime;
	}
	else if((data_lon != next_lon) || (data_lat != next_lat))
	{
		VecDur[data_lon][data_lat] = VecDur[data_lon][data_lat] + latter.gtime - temp_lon_dur;  
		temp_lon_dur = latter.gtime;
	}

	if(temp_pol_lat == -10000)
	{
		temp_pol_lat = pol_lat;
		temp_pol_lon = pol_lon;
		temp_pol_dur = data.gtime;
	}
	else if((pol_lon != pol_next_lon) || (pol_lat != pol_next_lat))
	{
		ArrPolDur[pol_lon][pol_lat] = ArrPolDur[pol_lon][pol_lat] + latter.gtime - temp_pol_dur;  
		temp_pol_dur = latter.gtime;
	}
}

/*
void AvePos()
{
	vector<int> vecAve;
	vecAve.clear();
	VecAve.clear();
	for(int i=0; i<VecDur.size(); i++)
	{
		for(int j=0; j<VecDur[i].size(); j++)
		{
			if(VecDur[i][j] != 0)
			{
				//int a = vecCoin[i]*(60000./(double)vecDur[i]);
				int ave = (int)(VecCoin[i][j]*(60000/(double)VecDur[i][j]));
				vecAve.push_back(ave);
			}
			else
			{
				vecAve.push_back(0);
			}
		}
		VecAve.push_back(vecAve);
		vecAve.clear();
	}
}
*/

void DrawPos()
{
	int counter;
	TH2D *h_pos = new TH2D("pos", "pos", Lon_bin, Lon_min, Lon_max, Lon_bin, Lat_min, Lat_max);
	h_pos->Sumw2();
	h_pos->SetStats(0);

	TH2D *h_posN = new TH2D("posN", "posN", Lon_bin, Lon_min, Lon_max, Lon_bin, Lat_min, Lat_max);
	h_posN->Sumw2();
	h_posN->SetStats(0);

	TH2D *h_pos_dur = new TH2D("pos_dur", "pos_dur", Lon_bin, Lon_min, Lon_max, Lon_bin, Lat_min, Lat_max);
	h_pos_dur->Sumw2();
	h_pos_dur->SetStats(0);

	TH2D *h_pol_dur = new TH2D("pol_dur", "pol_dur", pol_lon_bin, Lon_min, Lon_max, pol_lat_bin, Lat_min, Lat_max);
	h_pol_dur->Sumw2();
	h_pol_dur->SetStats(0);

/*	TH2D *h_posrate = new TH2D("posRate", "posRate", Lon_bin, Lon_min, Lon_max, Lon_bin, Lat_min, Lat_max);
	h_posrate->Sumw2();
	h_posrate->SetStats(0);
*/

	for(int i=0; i<VecCoin.size(); i++)
	{
		for(int j=0; j<VecCoin[i].size(); j++)
		{
			if(VecCoin[i][j] != 0)
			{
				//for(int k=0; k<VecCoin[i][j]; k++)
				//{ h_pos->Fill(Lon_min+(double)i*Lon_binw, Lat_min+(double)j*Lat_binw); }
				h_pos->SetBinContent(i+1, j+1, VecCoin[i][j]);
				h_posN->SetBinContent(i+1, j+1, VecCoinN[i][j]);
				h_pos_dur->SetBinContent(i+1, j+1, VecDur[i][j]);
				h_pos_dur->SetBinError(i+1, j+1, 0);
			}
			/*if(VecAve[i][j] != 0)
			{
				for(int k=0; k<VecAve[i][j]; k++)
				{ h_posrate->Fill(Lon_min+(double)i*Lon_binw, Lat_min+(double)j*Lat_binw); } 
			}*/
		}
	}

	for(int i=0; i<ArrPolDur.size(); i++)
	{
		for(int j=0; j<ArrPolDur[i].size(); j++)
		{
			if(ArrPolDur[i][j] != 0)
			{
				h_pol_dur->SetBinContent(i+1, j+1, ArrPolDur[i][j]);
				h_pol_dur->SetBinError(i+1, j+1, 0);
			}
		}
	}

	h_pos->Write();
	h_posN->Write();
	h_pos_dur->Write();
	h_pol_dur->Write();
	//h_posrate->Write();
	/*TH2D *h3 = new TH2D("posRateN", "posRateN", Lon_bin, Lon_min, Lon_max, Lon_bin, Lat_min, Lat_max);
	h3->Sumw2();
	h3->SetStats(0);*/

}

#endif
