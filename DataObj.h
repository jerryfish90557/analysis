#ifndef _DATAOBJ_H
#define _DATAOBJ_H

class Inf
{
public:
	vector<Long64_t> vecDur;
	vector<vector<Long64_t> > VecDur;

	vector<int> vecCoin;
	vector<vector<int> > VecCoin;

	vector<int> vecAve;
	vector<vector<int> > VecAve;

	vector<int> vecUpCome;
	vector<vector<int> > VecUpCome;
};

#endif
