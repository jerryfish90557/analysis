#ifndef _POLAR_H
#define _POLAR_H

#include <cmath>
#include <array>

double Astra_adjust = 180.;
double polar_adjust = 90.;

const int pol_lon_bin = 8;
const int pol_lat_bin = 9;
//array<array<int,pol_lat_bin>, pol_lon_bin> VecPolLon = {0};
//array<array<int,pol_lat_bin>, pol_lon_bin> VecPolLat = {0};
vector<vector<double> > VecPolLon(pol_lon_bin, vector<double>(pol_lat_bin,0.));
vector<vector<double> > VecPolLat(pol_lon_bin, vector<double>(pol_lat_bin,0.));
array<array<TH2D *, 9>, 8> hp0;
array<array<TH2D *, 9>, 8> hp1;
//TH2D *hp0[8][9];
//TH2D *hp1[8][9];

void PolarHistInitialize()
{
	double slip = 1e-11;
	for(int i=0; i<hp0.size(); i++)
	{
		for(int j=0; j<hp0[i].size(); j++)
		{
			hp0[i][j] = new TH2D(Form("pol_%d_%d_0",i,j),Form("pol_%d_%d_0",i,j),1,-M_PI,M_PI+slip,6,-M_PI,M_PI+slip);
			hp1[i][j] = new TH2D(Form("pol_%d_%d_1",i,j),Form("pol_%d_%d_1",i,j),7,-7.*M_PI/6.,7.*M_PI/6.+slip,3,-M_PI,M_PI+slip);
			//hp0[i][j] = new TH2D(Form("pol_%d_%d_0",i,j),Form("pol_%d_%d_0",i,j),1,-M_PI,M_PI+slip,12,-M_PI,M_PI+slip);
			//hp1[i][j] = new TH2D(Form("pol_%d_%d_1",i,j),Form("pol_%d_%d_1",i,j),7,-7.*M_PI/6.,7.*M_PI/6.+slip,5,-M_PI*5/6,M_PI*5/6+slip);

			hp0[i][j]->Sumw2();
			hp1[i][j]->Sumw2();
			hp0[i][j]->SetStats(0);
			hp1[i][j]->SetStats(0);
		}
	}
}

void polar(vector<double> &vecBestScan, int &pol_lon, int &pol_lat, double adjust, double &Nfactor)
{
	double phi = -(vecBestScan[2] - adjust - Astra_adjust) + polar_adjust;
	if(phi > 180.)
	{ phi = -(360. - phi); }
	else if(phi < -180.)
	{ phi = 360. + phi; }
	else
	{ phi = phi; }

	if(vecBestScan[1]<=30.)
	{ hp0[pol_lon][pol_lat]->Fill(phi*DegToRad, vecBestScan[1]*DegToRad*2, Nfactor); }
	else
	{ hp1[pol_lon][pol_lat]->Fill(phi*DegToRad, vecBestScan[1]*DegToRad*2, Nfactor); }

	/*if(vecBestScan[1]<=30.)
	{ hp0[pol_lon][pol_lat]->Fill(phi*DegToRad, vecBestScan[1]*DegToRad, Nfactor); }
	else
	{ hp1[pol_lon][pol_lat]->Fill(phi*DegToRad, vecBestScan[1]*DegToRad, Nfactor); }*/
}

void DrawPolar()
{
	for(int i=0; i<hp0.size(); i++)
	{
		for(int j=0; j<hp0[i].size(); j++)
		{
			hp0[i][j]->Write();
			hp1[i][j]->Write();
		}
	}
}

double AdjustPhi(double &ini_lon, double &ini_lat, double end_lon, double end_lat)
{
	double phi;
	double numerator = sin((end_lon-ini_lon)*DegToRad) * cos(end_lat*DegToRad);
	double dominator = cos(ini_lat*DegToRad) * sin(end_lat*DegToRad) - sin(ini_lat*DegToRad) * cos(end_lat*DegToRad) * cos((end_lon-ini_lon)*DegToRad);
	
	phi = atan2(numerator,dominator)*RadToDeg;

	return phi;
}

/*void polar(int &lon, int &lat, vector<double> &vecBestScan)
{
	int fill1, fill2;
	TH2D *h[2][2][2];
	double slip = 1e-11;
	for(int i=0; i<2; i++)
	{
		for(int j=0; j<2; j++)
		{
			h[i][j][0] = new TH2D(Form("h0_%d_%d",i,j),Form("h0_%d_%d",i,j),2,-2*M_PI,2*M_PI+slip,24,-2*M_PI,2*M_PI+slip);
			h[i][j][1] = new TH2D(Form("h1_%d_%d",i,j),Form("h1_%d_%d",i,j),12,-2*M_PI,2*M_PI+slip,5,-M_PI*5/6,M_PI*5/6+slip);

			h[i][j][0]->Sumw2();
			h[i][j][0]->SetStats();
			h[i][j][1]->Sumw2();
			h[i][j][1]->SetStats();
		}
	}

	for()
}*/

#endif
