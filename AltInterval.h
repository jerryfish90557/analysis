#ifndef _ALTINTERVAL_H
#define _ALTINTERVAL_H

#define Alt_bin 24
#define Alt_itv 500

#include <TH1F.h>
#include <TH2D.h>
#include "StrangeCh.h"
#include "LoadFile.h"
#include "track/track.C"

bool strange = true;
vector<int> vecStrange = {16, 31};
vector<int> vecNewCh;
bool pass;

extern vector<vector<double> > VecChannelPos;
extern vector<Long64_t> vecDur;
extern vector<int> vecCoin;
extern vector<int> vecAve;
Long64_t temp_dur = 0;
int temp_alt = -1;

void AltInterval(Astra &data, Astra &latter, vector<int> &vecChID)
{
	int alt = data.Alt/Alt_itv;
	int next_alt = latter.Alt/Alt_itv;
	
	if(strange == true)
	{
		StrangeCh(vecChID, vecStrange, vecNewCh);
		if(vecNewCh.size()>=2)
		{
			int trig_num = vecNewCh.size();
			pass = track(vecNewCh, VecChannelPos, trig_num);
			if(pass == true)
			{ vecCoin[alt]++; }
		}
	}
	else
	{
		if(vecChID.size()>=2)
		{
			int trig_num = vecChID.size();
			pass = track(vecChID, VecChannelPos, trig_num);
			if(pass == true)
			{ vecCoin[alt]++; }
		}
	}

	if(temp_alt == -1)
	{
		temp_dur = data.gtime;
		temp_alt = alt;
	}
	else if(alt != next_alt)
	{
		vecDur[alt] = vecDur[alt] + latter.gtime - temp_dur;
		temp_dur = latter.gtime;
	}
}

/*
void AveAlt()
{
	vecAve.clear();
	for(int i=0; i<vecDur.size(); i++)
	{
		if(vecDur[i] != 0)
		{
			//int a = vecCoin[i]*(60000./(double)vecDur[i]);
			int ave = (int)(vecCoin[i]*(60000/(double)vecDur[i]));
			vecAve.push_back(ave);
		}
		else
		{
			vecAve.push_back(0);
		}
	}
}
*/

void DrawAlt()
{
	int Alt_max = Alt_itv * Alt_bin;
	TH1F *h_event = new TH1F("event", "event", Alt_bin, 0, Alt_max);
	h_event->Sumw2();
	h_event->SetStats(0);
	
	TH1F *h_alt_dur = new TH1F("alt_dur", "alt_dur", Alt_bin, 0, Alt_max);
	h_alt_dur->Sumw2();
	h_alt_dur->SetStats(0);

	for(int i=0; i<vecCoin.size(); i++)
	{
		if(vecCoin[i] != 0)
		{
			for(int j=0; j<vecCoin[i]; j++)
			{
				h_event->Fill(i*Alt_itv);
			}

			h_alt_dur->SetBinContent(i+1, vecDur[i]);
			h_alt_dur->SetBinError(i+1, 0);
		}
	}

	h_event->Write();
	h_alt_dur->Write();
}
#endif
