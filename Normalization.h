#ifndef _NORMALIZATION_H
#define _NORMALIZATION_H

#include <cmath>

double Normalize(int &x)
{
	double pol4 = -1.17E-13*pow(x,4)+2.11E-9*pow(x,3)-1.60E-7*pow(x,2)-1.295E-3*x+81.24;	//for 202108
	//double pol4 = -2.37E-13*pow(x,4)+4.63E-9*pow(x,3)-1.74E-5*pow(x,2)+4.43E-2*x+47.5;	//for 20211124
	//double pol4 = -2.15E-13*pow(x,4)+3.99E-9*pow(x,3)-1.34E-5*pow(x,2)+3.60E-2*x+43.37;	//for 20220215
	return pol4;
}

#endif
