#ifndef _LOADCHANNEL_H
#define _LOADCHANNEL_H

#include <fstream>
#include <sstream>

string filename = "channelTable.txt";
vector<vector<double> > VecChannelPos;

void LoadChannel()
{
	vector<double> vecPos;
	ifstream file;
	cout << "Open the file ..." << endl;
	file.open(filename);

	if(file.fail())
	{
		cout << "Failed to laod Channel Table ..." << endl;
	}

	while(!file.eof())
	{
		string temp;
		getline(file, temp);
		if(temp.size() == 0)
		{
			continue;
		}
		istringstream stream(temp);

		while(1)
		{
			string ch, pos;
			stream >> ch;
			while(1)
			{
				if(!stream)
				{
					break;
				}
				stream >> pos;
				double strtoa = atof(pos.c_str());
				vecPos.push_back(strtoa);
			}
			VecChannelPos.push_back(vecPos);
			vecPos.clear();
			stream.clear();
			break;
		}		
	}
	file.close();
}

#endif
