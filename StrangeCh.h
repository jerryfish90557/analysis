#ifndef _STRANGECH_H
#define _STRANGECH_H

void StrangeCh(vector<int> &dataChID, vector<int> &vecStrange, vector<int> &vecCh)
{
	bool check = true;
	vecCh.clear();
	for(int i=0; i<dataChID.size(); i++)
	{
		for(int j=0; j<vecStrange.size(); j++)
		{
			if(dataChID[i] == vecStrange[j])
			{ check = false; }
		}
		
		if(check == true)
		{ vecCh.push_back(dataChID[i]); }
		else
		{ check = true; }
	}
}

void StrangeCh(vector<int> &dataChID, vector<int> &vecStrange, vector<double> &dataCharge, vector<double> &vecCharge)
{
	for(int i=0; i<dataChID.size(); i++)
	{
		for(int j=0; j<vecStrange.size(); j++)
		{
			if(dataChID[i] != vecStrange[j])
			{
				vecCharge.push_back(dataCharge[i]);
			}
		}
	}
}

#endif
