#ifndef _CHECKPASS_H
#define _CHECKPASS_H

#include "GetScinRange.h"

bool CheckPass(vector<double> &vecScanVec, vector<vector<double> > &VecHitPos, vector<double> &vecBotPos, double &ScanBot_x, double &ScanBot_y)
{

	vector<bool> vecCheck;
	for(int i=0; i<VecHitPos.size(); i++)
	{
		if(VecHitPos[i][3] == vecBotPos[3])
		{ continue; }
		
		vector<double> vecRange;
		GetScinRange(VecHitPos[i][3], vecRange);
		
		double max_x = VecHitPos[i][0] + vecRange[0];
		double min_x = VecHitPos[i][0] - vecRange[0];
		double max_y = VecHitPos[i][1] + vecRange[1];
		double min_y = VecHitPos[i][1] - vecRange[1];
	
		double x,y,func;
		bool check = false;
		if(VecHitPos[i][2] != vecBotPos[2])
		{
			for(double j=VecHitPos[i][2]-vecRange[2]; j<=VecHitPos[i][2]+vecRange[2]; j=j+1.)
			{
				func = (j-vecBotPos[2]+5.)/ vecScanVec[2];
				x = func * vecScanVec[0] + ScanBot_x;
				y = func * vecScanVec[1] + ScanBot_y;
			
				if((x<=max_x && x>=min_x) && (y<=max_y && y>=min_y))
				{
					check = true;
					break;
				}
			}
		}
		else
		{
			for(double j=VecHitPos[i][2]-vecRange[2]+1.; j<=VecHitPos[i][2]+vecRange[2]; j=j+1.)
			{
				func = (j-vecBotPos[2]+5.) / vecScanVec[2];
				x = func * vecScanVec[0] + ScanBot_x;
				y = func * vecScanVec[1] + ScanBot_y;
				
				if((x<=max_x && x>=min_x) && (y<=max_y && y>=min_y))
				{
					check = true;
					break;
				}
			}
		}
		vecCheck.push_back(check);
		vecRange.clear();
	}

	bool passAll = true;
	if(vecCheck.size()==1)
	{ passAll = vecCheck[0]; }
	else
	{
		for(int i=0; i<vecCheck.size()-1; i++)
		{
			passAll = vecCheck[i] & vecCheck[i+1];
			if(passAll == false)
			{
				break;
			}
		}
	}

	return passAll;
	vecCheck.clear();
}

#endif
