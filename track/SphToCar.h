#ifndef _SPHTOCAR_H
#define _SPHTOCAR_H

#include <cmath>

#define DegToRad M_PI/180.

void SphToCar(vector<double> &vecScanSph, vector<double> &vecScanVec)
{
	double x = vecScanSph[0]*sin(vecScanSph[1]*DegToRad)*cos(vecScanSph[2]*DegToRad);
	double y = vecScanSph[0]*sin(vecScanSph[1]*DegToRad)*sin(vecScanSph[2]*DegToRad);
	double z = vecScanSph[0]*cos(vecScanSph[1]*DegToRad);
	
	vecScanVec.push_back(x);
	vecScanVec.push_back(y);
	vecScanVec.push_back(z);

	//cout << "x: " << x << " y: " << y << " z: " << z << endl;
}

#endif
