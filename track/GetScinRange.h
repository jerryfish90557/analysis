#ifndef _GETSCINRANGE_H
#define _GETSCINRANGE_H

void GetScinRange(double &chID, vector<double> &vecRange)
{
	double x,y;
	double z = 5.;
	chID = (int)chID;
	if((chID >= 0 && chID < 8) || (chID >= 16 && chID < 24 ))
	{
		x = 20.;
		y = 5.;
	}
	else if((chID >= 8 && chID < 16) || (chID >= 24 && chID < 32))
	{
		x = 5.;
		y = 20;
	}
	else
	{
		cout << "GetRange error when loading channel ..." << endl;
	}
	
	vecRange.push_back(x);
	vecRange.push_back(y);
	vecRange.push_back(z);
}

#endif
