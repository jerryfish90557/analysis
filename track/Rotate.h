#ifndef _ROTATE_H
#define _ROTATE_H

#include <cmath>

#define DegToRad M_PI/180.
void Rotate(vector<double> &vecDirSph, vector<double> &vecScanVec)
{
	double theta = vecDirSph[1]*DegToRad;
	double phi = vecDirSph[2]*DegToRad;

	double x = cos(theta)*cos(phi)*vecScanVec[0] - sin(phi)*vecScanVec[1] + sin(theta)*cos(phi)*vecScanVec[2];
	double y = cos(theta)*sin(phi)*vecScanVec[0] + cos(phi)*vecScanVec[1] + sin(theta)*sin(phi)*vecScanVec[2];
	double z = -sin(theta)*vecScanVec[0] + cos(theta)*vecScanVec[2];
	
	vecScanVec.clear();
	vecScanVec.push_back(x);
	vecScanVec.push_back(y);
	vecScanVec.push_back(z);

//	cout << "x: " << x << " y: " << y << " z: " << z << endl;
}

#endif
