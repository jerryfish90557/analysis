#ifndef _APPLYPOS_H
#define _APPLYPOS_H

void ApplyPos(vector<int> &vecHit, vector<vector<double> > &VecChannelPos, vector<vector<double> > &VecHitPos)
{
	VecHitPos = {};
	vector<double> vecPos;
	for(int i=0; i<vecHit.size(); i++)
	{
		vecPos.push_back(VecChannelPos[vecHit[i]][0]);
		vecPos.push_back(VecChannelPos[vecHit[i]][1]);
		vecPos.push_back(VecChannelPos[vecHit[i]][2]);
		vecPos.push_back(vecHit[i]);

		VecHitPos.push_back(vecPos);
		vecPos.clear();
	}
}

#endif
