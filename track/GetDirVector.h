#ifndef _GETDIRVEC_H
#define _GETDIRVEC_H

#define top_z 39
#define bot_z 6

bool CheckSame(vector<vector<double> > &VecHitPos)
{
	for(int i=0; i<VecHitPos.size()-1; i++)
	{
		if(VecHitPos[i][3]!=VecHitPos[i+1][3])
		{
			return false;
		}
	}
	
	return true;
}


bool GetDirVec(vector<vector<double> > &VecHitPos, vector<double> &vecDirVec, vector<double> &vecBotPos)
//void GetDirVec(vector<vector<double> > &VecHitPos, vector<double> &vecDirVec, vector<double> &vecBotPos)
{
	bool check = CheckSame(VecHitPos);
	if(check==true)
	{ return check; }

	int top, bot;
	for(int i=0; i<VecHitPos.size(); i++)
	{
		if(i == 0)
		{
			if(VecHitPos[i][2] < VecHitPos[i+1][2])
			{
				top = i+1;
				bot = i;
			}
			else
			{
				top = i;
				bot = i+1;
			}
		}
		else
		{
			if(VecHitPos[i][2] < VecHitPos[bot][2])
			{
				bot = i;
			}
			else if(VecHitPos[i][2] > VecHitPos[top][2])
			{
				top = i;
			}
		}

		if(VecHitPos[top][2] == top_z && VecHitPos[bot][2] == bot_z)
		{ break; }
	}
	
	vecBotPos.clear();
	vecBotPos.push_back(VecHitPos[bot][0]);
	vecBotPos.push_back(VecHitPos[bot][1]);
	vecBotPos.push_back(VecHitPos[bot][2]);
	vecBotPos.push_back(VecHitPos[bot][3]);

	vecDirVec.clear();
	vecDirVec.push_back(VecHitPos[top][0]-vecBotPos[0]);
	vecDirVec.push_back(VecHitPos[top][1]-vecBotPos[1]);
	vecDirVec.push_back(VecHitPos[top][2]-vecBotPos[2]);

	return check;
}

#endif
