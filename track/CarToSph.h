#ifndef _CARTOSPH_H
#define _CARTOSPH_H

#include <cmath>

#define RadToDeg 180./M_PI

void CarToSph(vector<double> &vecDirVec, vector<double> &vecDirSph)
{
	double theta = atan2(hypot(vecDirVec[0],vecDirVec[1]),vecDirVec[2])*RadToDeg;
	double phi = atan2(vecDirVec[1],vecDirVec[0])*RadToDeg;

	vecDirSph.clear();
	vecDirSph.push_back(1.);
	vecDirSph.push_back(theta);
	vecDirSph.push_back(phi);
//	cout << "r: " << r << " theta: " << theta << " phi: " << phi << endl;
}

#endif
