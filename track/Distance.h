#ifndef _DISTANCE_H
#define _DISTANCE_H

#include <cmath>
#include "CheckPass.h"
#include "GetScinRange.h"

double GetDistance(vector<double> &vecScanVec, vector<vector<double> > &VecHitPos, vector<double> &vecBotPos)
{
	vector<double> vecBotRange;
	GetScinRange(vecBotPos[3], vecBotRange);

	const double z = vecBotPos[2] - vecBotRange[2];
	double add_vecScan = pow(vecScanVec[0],2) + pow(vecScanVec[1],2) +pow(vecScanVec[2],2);

	bool check, checkIfAnyPass = false;
	double distance = 0.;
	double temp = 99999.;
	for(double i=vecBotPos[0]-vecBotRange[0]; i<=vecBotPos[0]+vecBotRange[0]; i=i+1.)
	{
		for(double j=vecBotPos[1]-vecBotRange[1]; j<=vecBotPos[1]+vecBotRange[1]; j=j+1.)
		{
			check = CheckPass(vecScanVec, VecHitPos, vecBotPos, i, j);
			if(check == true)
			{
				checkIfAnyPass = true;
				for(int k=0; k<VecHitPos.size(); k++)
				{
					double t = -((i-VecHitPos[k][0])*vecScanVec[0] + (j-VecHitPos[k][1])*vecScanVec[1] + (z-VecHitPos[k][2])*vecScanVec[2]) / add_vecScan;
					double x_term = t*vecScanVec[0]+(i-VecHitPos[k][0]);
					double y_term = t*vecScanVec[1]+(j-VecHitPos[k][1]);
					double z_term = t*vecScanVec[2]+(z-VecHitPos[k][2]);

					distance = sqrt(pow(x_term,2)+pow(y_term,2)+pow(z_term,2)) + distance;
				}

				if((i == vecBotPos[0]-vecBotRange[0]) && (j == vecBotPos[1]-vecBotRange[1]))
				{ temp = distance; }

				if(temp > distance)
				{ temp = distance; }

				distance = 0.;
			}
		}
	}

	if(checkIfAnyPass == true)
	{ return temp; }
	else
	{ return 99999.; }
}

#endif
