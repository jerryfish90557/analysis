#ifndef _LOADFILE_H
#define _LOADFILE_H

#include <TFile.h>
#include <TTree.h>

#define datafile "dataFile.root"

TFile *readroot = new TFile(datafile, "read");
TFile *outputroot = new TFile("hist.root", "RECREATE");

TTree *tree = (TTree*)readroot->Get("data");

class Astra
{
	public:
		vector<int> *Channel = new vector<int>;
		vector<double> *Charge = new vector<double>;
		vector<double> *Time = new vector<double>;
		double Lat, Lon;
		int Alt;
		Long64_t gtime;
};

#endif
